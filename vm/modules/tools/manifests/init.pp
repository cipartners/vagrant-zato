class tools {
    $packages = ['curl', 'build-essential','apt-transport-https','python-software-properties', 'software-properties-common','python-dateutil']
    
    package { $packages:
        ensure => installed,
    }
}
